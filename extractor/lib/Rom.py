import zlib

"""
Identify the ROM game and version based on the CRC and instantiate the
appropriate Rom class.
"""
def load(filename):
    fp = open(filename, 'rb')
    fp.seek(0x10)
    bytes = fp.read(8)
    crc = int.from_bytes(bytes, 'big')

    if crc == 0xa24f4cf1a82327ba:
        rom = GeJapFinalRom()
    elif crc == 0xdcbc50d109fd1aa3:
        rom = GeNtscFinalRom()
    elif crc == 0x0414ca612e57b8aa:
        rom = GePalFinalRom()
    elif crc == 0x96747eb4104bb243:
        rom = PdJap89FinalRom()
    elif crc == 0x2ce75aefe16825bc:
        rom = PdNtsc64BetaRom()
    elif crc == 0x41f2b98fb458b466:
        rom = PdNtsc87FinalRom()
    elif crc == 0xf9864452890a5ea3:
        rom = PdPal287BetaRom()
    elif crc == 0xe4b08007a602ff33:
        rom = PdPal87FinalRom()
    else:
        print('Unrecognised ROM game/version')
        exit(1)

    rom.setFp(fp)
    return rom

class Rom:
    fp = None
    mdf = None

    def setFp(self, fp):
        self.fp = fp
        return self

    def getGame(self):
        return self.GAME

    """
    Read and extract the 0x21990 (GE) or 0x39850 (PD) data file and store it in
    self.mdf.
    """
    def loadMainDataFile(self):
        self.fp.seek(self.MDF_ROM_ADDRESS)
        buffer = self.fp.read(200000)
        self.mdf = self.inflate(buffer)

    """
    Read and return the common functions and stage-specific functions.
    We just return the functions as a block of bytes and don't try to read the
    individual instructions here.
    """
    def getFunctions(self):
        self.loadMainDataFile()
        functions = self.readFunctions(self.mdf, self.GLOBAL_FUNCTIONS_ADDRESS, self.MDF_MEM_ADDRESS, 0)
        for index, file_id in enumerate(self.SETUP_FILE_IDS):
            setup_file = self.getFile(file_id)
            functions = functions + self.readStageFunctions(setup_file, index + 1)
        return functions

    """
    Look up the file ID in the MDF's file list, then return a buffer containing
    the file data. The file may be compressed.
    """
    def getFile(self, file_id):
        offset = self.FILELIST_ADDRESS + file_id * self.FILE_ENTRY_LENGTH + self.FILE_ENTRY_OFFSET
        next = offset + self.FILE_ENTRY_LENGTH
        start_address = int.from_bytes(self.mdf[offset:offset+4], 'big')
        end_address = int.from_bytes(self.mdf[next:next+4], 'big')
        self.fp.seek(start_address)
        return self.fp.read(end_address - start_address)

    def readStageFunctions(self, setup_file, stage_id):
        setup_file = self.inflate(setup_file)
        offset = self.SETUP_FUNCTIONS_OFFSET
        table_address = int.from_bytes(setup_file[offset:offset+4], 'big')
        return self.readFunctions(setup_file, table_address, 0, stage_id)

    """
    Read functions out of a buffer and return them as an array.

    table_address is expected to be a buffer-local address where the functions
    table starts. The table consists of entries of 8 bytes each. 4 bytes for the
    address and 4 bytes for the function ID.

    For stage functions, the address will be buffer-local. For the common
    functions, the address will be a memory address, so we subtract the
    data_offset argument to translate it into a buffer-local address.
    """
    def readFunctions(self, buffer, table_address, data_offset, stage_id):
        pairs = []
        while True:
            offset = int.from_bytes(buffer[table_address:table_address+4], 'big')
            function_id = int.from_bytes(buffer[table_address+4:table_address+8], 'big')
            if offset == 0:
                break
            pairs.append((function_id, offset))
            table_address += 8
        pairs = sorted(pairs, key=lambda x:x[1])
        functions = []
        for index, pair in enumerate(pairs):
            start = pair[1] - data_offset;
            end = table_address if index == len(pairs) - 1 else pairs[index + 1][1]
            end -= data_offset
            functions.append({
                'id': pair[0],
                'stage_id': stage_id,
                'raw': buffer[start:end],
            })
        return functions

    def inflate(self, buffer):
        return zlib.decompress(buffer[self.ZLIB_HEADER_LENGTH:], wbits=-15)

class GeRom(Rom):
    GAME = 'ge'
    ZLIB_HEADER_LENGTH = 2
    SETUP_FUNCTIONS_OFFSET = 0x14
    FILE_ENTRY_LENGTH = 0x0c
    FILE_ENTRY_OFFSET = 0x08
    SETUP_FILE_IDS = [
        0x0270, # Dam
        0x026a, # Facility
        0x0276, # Runway
        0x0279, # Surface 1
        0x0278, # Bunker 1
        0x027b, # Silo
        0x0272, # Frigate
        0x027a, # Surface 2
        0x0277, # Bunker 2
        0x027c, # Statue
        0x0269, # Archives
        0x0275, # Streets
        0x0271, # Depot
        0x027d, # Train
        0x0273, # Jungle
        0x026d, # Control
        0x026c, # Caverns
        0x026e, # Cradle
        0x026b, # Aztec
        0x026f, # Egypt
        0x0274, # Credits
    ]

class PdRom(Rom):
    GAME = 'pd'
    ZLIB_HEADER_LENGTH = 5
    SETUP_FUNCTIONS_OFFSET = 0x18
    FILE_ENTRY_LENGTH = 0x04
    FILE_ENTRY_OFFSET = 0x00
    SETUP_FILE_IDS = [
        0x0126, # Defection
        0x0134, # Investigation
        0x0129, # Extraction
        0x0135, # Villa
        0x013e, # Chicago
        0x0131, # G5 Building
        0x013b, # Infiltration
        0x013a, # Rescue
        0x0149, # Escape
        0x012d, # Air Base
        0x0140, # Air Force One
        0x012b, # Crash Site
        0x0130, # Pelagic II
        0x013d, # Deep Sea
        0x0136, # Defense
        0x0138, # Attack Ship
        0x0146, # Skedar Ruins
        0x014a, # MBR
        0x0142, # Maian SOS
        0x0148, # WAR!
        0x01d9, # Duel
        0x0133, # CI Training
        0x013c, # Skedar (mp)
    ]

"""
MDF_ROM_ADDRESS = address in ROM where the main data file starts
MDF_MEM_ADDRESS = address in N64 memory where the main data file is placed
FILELIST_ADDRESS = offset in the MDF where the file table starts
GLOBAL_FUNCTIONS_ADDRESS = offset in the MDF where the global functions list starts
"""

class GeJapFinalRom(GeRom):
    MDF_ROM_ADDRESS = 0x219d0
    MDF_MEM_ADDRESS = 0x80020dd0
    FILELIST_ADDRESS = 0x252b4
    GLOBAL_FUNCTIONS_ADDRESS = 0x166ac

class GeNtscFinalRom(GeRom):
    MDF_ROM_ADDRESS = 0x21990
    MDF_MEM_ADDRESS = 0x80020d90
    FILELIST_ADDRESS = 0x252c4
    GLOBAL_FUNCTIONS_ADDRESS = 0x166bc

class GePalFinalRom(GeRom):
    MDF_ROM_ADDRESS = 0x1f850
    MDF_MEM_ADDRESS = 0x8001ec50
    FILELIST_ADDRESS = 0x1fe74
    GLOBAL_FUNCTIONS_ADDRESS = 0x138ac

class PdJap89FinalRom(PdRom):
    MDF_ROM_ADDRESS = 0x39850
    MDF_MEM_ADDRESS = 0x80059ea0
    FILELIST_ADDRESS = 0x28800
    GLOBAL_FUNCTIONS_ADDRESS = 0x213a8

class PdNtsc64BetaRom(PdRom):
    MDF_ROM_ADDRESS = 0x30850
    MDF_MEM_ADDRESS = 0x8005b760
    FILELIST_ADDRESS = 0x29160
    GLOBAL_FUNCTIONS_ADDRESS = 0x21d58

class PdNtsc87FinalRom(PdRom):
    MDF_ROM_ADDRESS = 0x39850
    MDF_MEM_ADDRESS = 0x80059fe0
    FILELIST_ADDRESS = 0x28080
    GLOBAL_FUNCTIONS_ADDRESS = 0x20c78

class PdPal287BetaRom(PdRom):
    MDF_ROM_ADDRESS = 0x39850
    MDF_MEM_ADDRESS = 0x8005a980
    FILELIST_ADDRESS = 0x29b90
    GLOBAL_FUNCTIONS_ADDRESS = 0x227a8

class PdPal87FinalRom(PdRom):
    MDF_ROM_ADDRESS = 0x39850
    MDF_MEM_ADDRESS = 0x80059c90
    FILELIST_ADDRESS = 0x28910
    GLOBAL_FUNCTIONS_ADDRESS = 0x21498
