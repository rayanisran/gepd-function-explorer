import lib.GeRegistry
import lib.PdRegistry
import jinja2, math, random, re

def load(game):
    if game == 'ge':
        return GeGenerator()
    else:
        return PdGenerator()

class Generator:
    functions = None
    anno_dir = None
    output_dir = None
    stages = None
    colors = None
    next_color_index = 0
    label_colors = {}
    ge_stages = [
        {'shortname': 'global',   'fullname': 'Global'},
        {'shortname': 'dam',      'fullname': 'Dam'},
        {'shortname': 'facility', 'fullname': 'Facility'},
        {'shortname': 'runway',   'fullname': 'Runway'},
        {'shortname': 'surface1', 'fullname': 'Surface 1'},
        {'shortname': 'bunker1',  'fullname': 'Bunker 1'},
        {'shortname': 'silo',     'fullname': 'Silo'},
        {'shortname': 'frigate',  'fullname': 'Frigate'},
        {'shortname': 'surface2', 'fullname': 'Surface 2'},
        {'shortname': 'bunker2',  'fullname': 'Bunker 2'},
        {'shortname': 'statue',   'fullname': 'Statue'},
        {'shortname': 'archive',  'fullname': 'Archives'},
        {'shortname': 'streets',  'fullname': 'Streets'},
        {'shortname': 'depot',    'fullname': 'Depot'},
        {'shortname': 'train',    'fullname': 'Train'},
        {'shortname': 'jungle',   'fullname': 'Jungle'},
        {'shortname': 'control',  'fullname': 'Control'},
        {'shortname': 'caverns',  'fullname': 'Caverns'},
        {'shortname': 'cradle',   'fullname': 'Cradle'},
        {'shortname': 'aztec',    'fullname': 'Aztec'},
        {'shortname': 'egypt',    'fullname': 'Egypt'},
        {'shortname': 'credits',  'fullname': 'Credits'},
    ]
    pd_stages = [
        {'shortname': 'global',        'fullname': 'Global'},
        {'shortname': 'defection',     'fullname': 'Defection'},
        {'shortname': 'investigation', 'fullname': 'Investigation'},
        {'shortname': 'extraction',    'fullname': 'Extraction'},
        {'shortname': 'villa',         'fullname': 'Villa'},
        {'shortname': 'chicago',       'fullname': 'Chicago'},
        {'shortname': 'g5building',    'fullname': 'G5 Building'},
        {'shortname': 'infiltration',  'fullname': 'Infiltration'},
        {'shortname': 'rescue',        'fullname': 'Rescue'},
        {'shortname': 'escape',        'fullname': 'Escape'},
        {'shortname': 'airbase',       'fullname': 'Air Base'},
        {'shortname': 'airforceone',   'fullname': 'Air Force One'},
        {'shortname': 'crashsite',     'fullname': 'Crash Site'},
        {'shortname': 'pelagic2',      'fullname': 'Pelagic II'},
        {'shortname': 'deepsea',       'fullname': 'Deep Sea'},
        {'shortname': 'defense',       'fullname': 'Defense'},
        {'shortname': 'attackship',    'fullname': 'Attack Ship'},
        {'shortname': 'skedarruins',   'fullname': 'Skedar Ruins'},
        {'shortname': 'mbr',           'fullname': 'MBR'},
        {'shortname': 'maiansos',      'fullname': 'Maian SOS'},
        {'shortname': 'war',           'fullname': 'WAR!'},
        {'shortname': 'duel',          'fullname': 'Duel'},
        {'shortname': 'citraining',    'fullname': 'CI Training'},
        {'shortname': 'skedarmp',      'fullname': 'Skedar (mp)'},
    ]

    def setFunctions(self, functions):
        self.functions = functions
        return self

    def setAnnotationsDirectory(self, dir):
        self.anno_dir = dir
        return self;

    def setOutputDirectory(self, dir):
        self.output_dir = dir
        return self;

    def generate(self):
        self.stages = self.getStages()
        self.annotateStages()
        self.annotateFunctions()
        for stage in self.stages:
            self.generateStageFile(stage)
        for function in self.functions:
            self.generateFunctionFile(function)

    def annotateStages(self):
        for stage_id, stage in enumerate(self.ge_stages):
            stage['id'] = stage_id
            stage['url'] = 'ge-%s.html' % stage['shortname']

        for stage_id, stage in enumerate(self.pd_stages):
            stage['id'] = stage_id
            stage['url'] = 'pd-%s.html' % stage['shortname']

        for stage_id, stage in enumerate(self.stages):
            stage['first_function'] = next(function for function in self.functions if function['stage_id'] == stage_id)

    def annotateFunctions(self):
        for function in self.functions:
            self.annotateFunction(function)

        # Link up prev/next elements
        for index, function  in enumerate(self.functions):
            try:
                if self.functions[index + 1]['stage_id'] == function['stage_id']:
                    function['next'] = self.functions[index + 1]
                    self.functions[index + 1]['prev'] = function
                else:
                    function['next'] = None
                    self.functions[index + 1]['prev'] = None
            except:
                function['next'] = None

    def annotateFunction(self, function):
        filename = '%s/%s-%02d-%04x.md' % (self.anno_dir, self.GAME, function['stage_id'], function['id'])
        function['url'] = '%s-%s-%04x.html' % (self.GAME, self.stages[function['stage_id']]['shortname'], function['id'])
        function['title'] = '%04x' % function['id']
        function['comments'] = None
        try:
            fp = open(filename, 'r')
        except:
            return
        title = fp.readline().strip()
        if title: function['title'] = '%04x - %s' % (function['id'], title)
        function['comments'] = ''.join(fp.readlines()).strip()
        fp.close()

    def generateStageFile(self, stage):
        env = jinja2.Environment(
            loader=jinja2.FileSystemLoader('extractor/templates'),
            autoescape='html'
        )
        template = env.get_template('stage-overview.html')

        actors = []
        try:
            ss = self.registry.stage_specific[stage['id']]
            for key in ss['actors']:
                actor = ss['actors'][key]
                actors.append({
                    'id': key,
                    'name': actor['name'],
                    'function': self.findFunction(actor['function'], stage),
                })
        except:
            pass

        objects = []
        try:
            ss = self.registry.stage_specific[stage['id']]
            for key in ss['objects']:
                obj = ss['objects'][key]
                objects.append({
                    'id': key,
                    'name': obj['name'],
                    'function': self.findFunction(obj['function'], stage) if 'function' in obj else None,
                })
        except:
            pass

        html = template.render(
                title=stage['fullname'],
                game=self.GAME,
                ge_stages=self.ge_stages,
                pd_stages=self.pd_stages,
                stage=stage,
                all_functions=self.functions,
                functions=[f for f in self.functions if f['stage_id'] == stage['id']],
                actors=actors,
                objects=objects
        )

        filename = "%s/%s" % (self.output_dir, stage['url'])
        fp = open(filename, 'w')
        fp.write(html)
        fp.close()

    def findFunction(self, function_id, stage):
        if function_id < 0x0400:
            return next(f for f in self.functions if f['id'] == function_id)
        return next(f for f in self.functions if f['id'] == function_id and f['stage_id'] == stage['id'])

    def generateFunctionFile(self, function):
        self.makeColors()
        env = jinja2.Environment(
            loader=jinja2.FileSystemLoader('extractor/templates'),
            autoescape='html'
        )
        template = env.get_template('function.html')

        stage = self.stages[function['stage_id']]
        stage_functions = [f for f in self.functions if f['stage_id'] == stage['id']]
        self.label_colors = {}

        for instruction in function['instructions']:
            self.annotateInstruction(instruction)
            if instruction['type'] == 0x02:
                self.makeLabel(instruction)

        try:
            html = template.render(
                    title=function['title'],
                    game=self.GAME,
                    ge_stages=self.ge_stages,
                    pd_stages=self.pd_stages,
                    stage=stage,
                    function=function,
                    stage_functions=stage_functions,
                    col_len=math.ceil(len(stage_functions)/3),
                    invocations=self.findInvocations(function),
                    generator=self
            )
        except Exception as e:
            print('%s - %s' % (stage['fullname'], function['title']))
            raise e

        filename = "%s/%s" % (self.output_dir, function['url'])
        fp = open(filename, 'w')
        fp.write(html)
        fp.close()

    def annotateInstruction(self, instruction):
        if instruction['type'] in self.handlers:
            instruction['handler'] = self.handlers[instruction['type']]

    def makeLabel(self, instruction):
        label = int.from_bytes(instruction['params'], 'big')
        if label not in self.label_colors:
            self.label_colors[label] = self.colors[self.next_color_index]
            if self.next_color_index == len(self.colors) - 1:
                self.next_color_index = 0
            else:
                self.next_color_index += 1

    def findInvocations(self, function):
        invocations = []
        if function['id'] & 0xff00 == 0x0000:
            invocations.append('This function may be invoked from any other function in the game.')
            return
        invocations += self.findInvocationsAuto(function);
        invocations += self.findInvocationsInitialActors(function);
        invocations += self.findInvocationsInitialObjects(function);
        invocations += self.findInvocationsAssigned(function);
        return invocations

    def findInvocationsAuto(self, function):
        if function['id'] & 0xff00 == 0x1000:
            return ['Started automatically']
        if function['id'] & 0xff00 == 0x1400:
            return ['Started automatically']
        if function['id'] & 0xff00 == 0x0c00:
            return ['Started automatically when using cinema menu']
        return []

    def findInvocationsInitialActors(self, function):
        if not function['stage_id'] in self.registry.stage_specific:
            return []
        invocations = []
        actors = self.registry.stage_specific[function['stage_id']]['actors']
        for actor_index in actors:
            actor = actors[actor_index]
            if actor['function'] == function['id']:
                invocations.append('Initial function for actor %02x (%s)' % (actor_index, actor['name']))
        return invocations

    def findInvocationsInitialObjects(self, function):
        if not function['stage_id'] in self.registry.stage_specific:
            return []
        invocations = []
        objects = self.registry.stage_specific[function['stage_id']]['objects']
        for object_index in objects:
            obj = objects[object_index]
            if 'function' in obj and obj['function'] == function['id']:
                invocations.append('Initial function for object %02x (%s)' % (object_index, obj['name']))
        return invocations

    def findInvocationsAssigned(self, function):
        invocations = []
        text = '[%04x]' % function['id']
        for func in self.functions:
            if function['stage_id'] != func['stage_id']:
                continue
            if function['id'] == func['id']:
                continue
            for instruction in func['instructions']:
                if text in instruction['description']:
                    invocations.append('Can be invoked by function <a href="%s">%s</a>' % (func['url'], func['title']))
                    break
        return invocations

    def markupDescription(self, function, description):
        description = str(jinja2.escape(description))

        # Function links
        for func in self.functions:
            if func['stage_id'] != function['stage_id'] and func['stage_id'] != 0:
                continue
            description = description.replace('[%04x]' % func['id'], '<a href="%s">%s</a>' % (func['url'], jinja2.escape(func['title'])))

        # Goto labels
        description = re.sub(r'(go\s*to)( first| next)? ([a-z0-9]{2})$', self.markupGoto, description, flags=re.IGNORECASE)

        description = description.replace('\n', '<br>')
        return description

    def markupGoto(self, match):
        label = int(match.group(3), 16)
        first_or_empty = match.group(2) if match.group(2) else ''
        if label in self.label_colors:
            return '%s%s <span class="label" style="background:#%06x">%s</span>' % (match.group(1), first_or_empty, self.label_colors[label], match.group(3))
        else:
            return '%s%s <span class="label label-default">%s</span>' % (match.group(1), first_or_empty, match.group(3))

    def generateIndexFile(self):
        env = jinja2.Environment(
            loader=jinja2.FileSystemLoader('extractor/templates'),
            autoescape='html'
        )
        template = env.get_template('index.html')

        html = template.render(
                title='GE/PD Function Explorer',
                game=None,
                ge_stages=self.ge_stages,
                pd_stages=self.pd_stages,
                stage=None
        )

        filename = "%s/index.html" % self.output_dir
        fp = open(filename, 'w')
        fp.write(html)
        fp.close()

    def makeColors(self):
        self.colors = [];
        for r in [0, 0x800000, 0xff0000]:
            for g in [0, 0x8000, 0xff00]:
                for b in [0, 0x80, 0xff]:
                    color = r | g | b
                    if color not in [0xffffff, 0xffff80, 0xffff00, 0x80ffff]:
                        self.colors.append(color)
        random.shuffle(self.colors)

    def handleLabel(self, instruction, stage):
        label = int.from_bytes(instruction['params'], 'big')
        return '<span class="label" style="background:#%06x">Label %02x</span>' % (self.label_colors[label], label)

class GeGenerator(Generator):
    GAME = 'ge'
    registry = lib.GeRegistry
    handlers = {}

    def __init__(self):
        self.handlers = {
            0x02: self.handleLabel,
            0xc2: self.handlec2Speech,
            0xc3: self.handlec3Speech,
        }

    def getStages(self):
        return self.ge_stages

    def typeToHexString(self, type_id):
        return '%02x' % type_id

    def handlec2Speech(self, instruction, stage):
        text_id = int.from_bytes(instruction['params'], 'big')
        text = self.registry.texts[text_id] if text_id in self.registry.texts else '(unknown text)'
        return 'Display text at bottom left:<br><div class="speech speech-ge">%s</div>' % text

    def handlec3Speech(self, instruction, stage):
        text_id = int.from_bytes(instruction['params'], 'big')
        text = self.registry.texts[text_id] if text_id in self.registry.texts else '(unknown text)'
        return 'Display text at top:<br><div class="speech speech-ge">%s</div>' % text

class PdGenerator(Generator):
    GAME = 'pd'
    registry = lib.PdRegistry
    handlers = {}
    speech_colors = {
        0: 'green',
        1: 'blue',
        2: 'white',
        3: 'red',
        4: 'orange',
        5: 'green',
        6: 'white',
        7: 'red',
        8: 'orange',
        9: 'blue',
    }

    def __init__(self):
        self.handlers = {
            0x0002: self.handleLabel,
            0x00cb: self.handle00cbSpeech,
            0x00cc: self.handle00ccSpeech,
            0x00cd: self.handle00cdSpeech,
            0x01a4: self.handle01a4Speech,
        }

    def getStages(self):
        return self.pd_stages

    def typeToHexString(self, type_id):
        return '%04x' % type_id

    # 00cbaatttt
    def handle00cbSpeech(self, instruction, stage):
        actor_id = instruction['params'][0]
        actor = self.registry.actors[actor_id] if actor_id in self.registry.actors else self.registry.stage_specific[stage['id']]['actors'][actor_id]['name']
        text_id = int.from_bytes(instruction['params'][1:3], 'big')
        text = self.registry.texts[text_id] if text_id in self.registry.texts else '(unknown text)'
        return 'Display text in bottom left for %s:<br><div class="speech speech-green">%s</div>' % (actor, text)

    # 00ccaattttcc
    def handle00ccSpeech(self, instruction, stage):
        actor_id = instruction['params'][0]
        actor = self.registry.actors[actor_id] if actor_id in self.registry.actors else self.registry.stage_specific[stage['id']]['actors'][actor_id]['name']
        text_id = int.from_bytes(instruction['params'][1:3], 'big')
        text = self.registry.texts[text_id] if text_id in self.registry.texts else '(unknown text)'
        color_id = instruction['params'][3]
        color = self.speech_colors[color_id]
        return 'Display text in top middle for %s:<br><div class="speech speech-%s">%s</div>' % (actor, color, text)

    # 00cdggttttaaaabbcc
    def handle00cdSpeech(self, instruction, stage):
        actor_id = instruction['params'][0]
        actor = self.registry.actors[actor_id] if actor_id in self.registry.actors else self.registry.stage_specific[stage['id']]['actors'][actor_id]['name']
        text_id = int.from_bytes(instruction['params'][1:3], 'big')
        text = self.registry.texts[text_id] if text_id in self.registry.texts else '(unknown text)'
        channel = instruction['params'][5]
        color_id = instruction['params'][6]
        color = self.speech_colors[color_id]
        return 'Display text and play a sound in channel %d for %s:<br><div class="speech speech-%s">%s</div>' % (channel, actor, color, text)

    # 01a4??cctttt
    def handle01a4Speech(self, instruction, stage):
        color_id = instruction['params'][1]
        color = self.speech_colors[color_id]
        text_id = int.from_bytes(instruction['params'][2:4], 'big')
        text = self.registry.texts[text_id] if text_id in self.registry.texts else '(unknown text)'
        return 'Display text in top middle:<br><div class="speech speech-%s">%s</div>' % (color, text)

