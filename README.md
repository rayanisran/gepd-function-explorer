# GoldenEye and Perfect Dark Function Explorer

This repository contains the tools used to generate the [GE/PD Function Explorer](https://ryandwyer.gitlab.io/gepd-function-explorer) pages.

The tools are just a couple of Python 3 libraries. They work with all known versions of the GE and PD ROMs, including betas. The GE/PD Function Explorer pages are generated using the final NTSC versions of both games. The more curious of you may want to run the extractor with the PAL or JAP ROMs, and the betas to look for differences.

The extractor works by combining the annotations stored in the annotations directory with a ROM filename passed as an argument to the extractor script.

The ROMs themselves are not included in this repository.

## Where is your instruction list?

Look in [GeRegistry.py](extractor/lib/GeRegistry.py) and [PdRegistry.py](extractor/lib/PdRegistry.py). These files also contain much more than just the instructions, such as actor, object and weapon lists.

## How do I submit an annotation?

Make the appropriate change in the `annotations` directory, then submit a pull request.

The format of an annotation file is pretty simple. The first line is the title, and all subsequent lines are the comments.

## How do I run the extractor?

Your command should be something like:

    $ python extractor/extractor.py path/to/GoldenEye.z64

This will populate the `public` directory with HTML files, which you can open directly from disk (you don't need to set up a web server).

Note that your ROM must not be byteflipped (the first 4 bytes should be `0x80371240`).

The script may take a couple of minutes to run. It produces no output unless something goes wrong.

## How do I generate the HTML pages after making code changes?

You can run the above, or you can take a shortcut and run this:

    $ python extractor/mkpages.py

This script picks up the functions from `extractor/ge.json` and `extractor/pd.json`. This is what's executed by GitLab - the functions are only stored this way because we can't commit the ROMs to the repository for legal reasons.

You can also generate the pages for just one game, which saves time:

    $ python extractor/mkpages.py --ge

## How do I fix a bug or implement an unknown instruction?

All the good stuff is in [GeRegistry.py](extractor/lib/GeRegistry.py), [PdRegistry.py](extractor/lib/PdRegistry.py) and [the main parser](extractor/lib/Parser.py). Make a pull request.

